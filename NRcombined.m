function [c, mu, cdot] = NRcombined(c_prev_ts, cdot_pre_ts, mu_prev, dt, alpha, D, gamma, bigm, bigk, numele, node, manu_soln_on, x, t)
%initialize fields
c = c_prev_ts;
mu = mu_prev;
ucombined = [c; mu];
numnod = length(c);
iter = 0;
resid_norm = Inf;

TOL = 1e-9;
MAX_ITER = 30;
if manu_soln_on
   fprintf("NR-iteration\tresidual\n");
end

while resid_norm > TOL && iter < MAX_ITER
   iter = iter+1;
   
   % form residual
   Rc = assembleConcResid(D, bigm, bigk, mu, c, dt, c_prev_ts, cdot_pre_ts, alpha, numele, node, manu_soln_on, x, t);
   Ru = assembleMuResid(gamma, bigm, bigk, mu, c, dt, alpha, numele, node, x);
   Rcombined = [Rc; Ru];

   % form tangent stiffness matrix
   Ktan = getKtanCombined(c, bigm, bigk, numele, node, x, numnod, dt, D, gamma, alpha);
   
   % dirichlet conditions for manufactured solution
   if manu_soln_on
       for n=1:numnod
          if (n == 1 || n == numnod)
             Ktan(n,1:numnod*2) = zeros(1,numnod*2);
             Ktan(1:numnod*2,n) = zeros(numnod*2,1);
             Ktan(n,n) = 1.0;
             Rcombined(n) = 0.0;
          end
       end
   end
   
   % evaluate norm of residual, output result
   resid_norm = sqrt(sum(Rcombined .* Rcombined));
   if manu_soln_on
       fprintf("%d\t%e\n", iter, resid_norm);
   end
   
   % solve for increment in displacement
   delta_u = Ktan \ Rcombined;
   
   % update conc field
   ucombined = ucombined - delta_u;
   c = ucombined(1:numnod);
   mu = ucombined(numnod+1:end);
end

c = ucombined(1:numnod);
mu = ucombined(numnod+1:end);
fprintf("NR combined converged in %d steps\n", iter);
cdot = (c - c_prev_ts) / dt;
end