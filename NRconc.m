function [c, cdot] = NRconc(c_prev_ts, cdot_pre_ts, mu_prev, dt, alpha, D, gamma, me, ke, numele, node, x)
%initialize fields
% c = zeros(size(c_prev_ts));
c = c_prev_ts;
numnod = length(c);
iter = 0;
resid_norm = Inf;

TOL = 1e-10;
MAX_ITER = 30;

while resid_norm > TOL && iter < MAX_ITER
   iter = iter+1;
   
   % form residual
   Rc = assembleConcResid(D, me, ke, mu_prev, c, dt, c_prev_ts, cdot_pre_ts, alpha, numele, node, x);
%     Rc = assembleMuResid(gamma, me, ke, ne, mu_prev, c, dt, alpha, numele, node, x);

   % form tangent stiffness matrix
   Ktan = getKtanC(me, numele, node, x, numnod);
   Ktan = Ktan .* (1 / (alpha .* dt));
   
   % evaluate norm of residual, output result
   resid_norm = sqrt(sum(Rc .* Rc));
   
   % solve for increment in displacement
   delta_c = Ktan \ Rc;
   
   % update conc field
    c = c - delta_c;
end
fprintf("NR for concentration converged in %d steps\n", iter);
cdot = (c - c_prev_ts) / dt;
end