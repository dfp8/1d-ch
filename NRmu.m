function mu = NRmu(c, mu_prev, c_prev_ts, cdot_pre_ts, dt, alpha, D, gamma, bigm, bigk, numele, node, x)
%initialize fields
mu = mu_prev;
iter = 0;
resid_norm = Inf;

TOL = 1e-10;
MAX_ITER = 30;

while resid_norm > TOL && iter < MAX_ITER
   iter = iter+1;
   
   % form residual
   Ru = assembleMuResid(gamma, bigm, bigk, mu, c, dt, alpha, numele, node, x);

   % form tangent stiffness matrix
   Ktan = bigm;
   
   % evaluate norm of residual, output result
   resid_norm = sqrt(sum(Ru .* Ru));
   
   % solve for increment in displacement
   delta_mu = Ktan \ Ru;
   
   % update mu field
   mu = mu - delta_mu;
end
fprintf("NR for mu converged in %d steps\n", iter);
end