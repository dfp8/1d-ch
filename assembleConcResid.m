function Rc = assembleConcResid(D, bigm, bigk, mu, c, dt, c_prev_ts, cdot_pre_ts, alpha, numele, node, manu_soln_on, x, t)

bigf = zeros(size(c));

if manu_soln_on
    %ELEMENT LOOP
    for e=1:numele
        he = x(node(2,e)) - x(node(1,e));

        % calculate the element force
        f1 = getManufacturedForce(x(node(1,e)), 1, t);
        f2 = getManufacturedForce(x(node(2,e)), 1, t);
        fe = [he/6*(2*f1 + f2), he/6*(2*f2 + f1)]; %element force vector

        % assemble element force
        bigf(node(1,e)) = bigf(node(1,e)) + fe(1);
        bigf(node(2,e)) = bigf(node(2,e)) + fe(2);
    end
end

if alpha > 0
    cdot = (c - (c_prev_ts + (1 - alpha) .* dt .* cdot_pre_ts)) ...
            ./ (alpha .* dt);
else
    cdot = cdot_pre_ts;
end
Rc = bigm * cdot + D .* (bigk * mu) - bigf;

end