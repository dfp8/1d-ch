function Ru = assembleMuResid(gamma, bigm, bigk, mu, c, dt, alpha, numele, node, x)
numnod = length(c);
bigf = zeros(numnod, 1);

%ELEMENT LOOP
for e=1:numele
    he = x(node(2,e)) - x(node(1,e));

    jacobian = he ./ 2;
    % calculate the element force
    ce = [c(node(1,e)),  c(node(2,e))];
    fe = zeros(2, 1);
    locations = ((1 ./ sqrt(3)) .* [-1, 1]);
    weights = [1, 1];
    for eta_ind = 1:2
        eta = locations(eta_ind);
        N = getN(eta);
        ce_interp = ce * N';
        fe = fe + N' .* getDPhi(ce_interp) .* weights(eta_ind) .* jacobian;
    end

    % assemble element force
    bigf(node(1,e)) = bigf(node(1,e)) + fe(1);
    bigf(node(2,e)) = bigf(node(2,e)) + fe(2);
end

Ru = bigm * mu - bigf - gamma .* bigk * c;

end

function phi = getDPhi(c)
phi = c .^ 3 - c;
end