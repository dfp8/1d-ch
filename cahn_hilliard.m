clear;
clc;
close all
% INPUT DATA
% number of elements and nodes
numele = 300;
numnod = numele+1;
x = linspace(0,1,numnod);

%nodal connectivity (LM) array
node = [1:numele
   2:numele+1];

% diffusion and gamma
D = 1;
gamma = (1/32)^2;
alpha = 1;

% c_prev_ts = zeros(numnod, 1);
cdot_pre_ts = zeros(numnod, 1);
mu_prev = zeros(numnod, 1);
%initialize at first time step
% for i=1:numnod
%   c_prev_ts(i) = 0.5 + 0.01 .* sin(50 .* x(i));
% end
rng(42, 'twister');  % seed for reproducibility
c_prev_ts = (rand(numnod,1)-0.5)*0.01;

% dt = 5e-6;
dt = 1e-4;
num_tsteps = 1700;
WRITE_VIDEO = false;
% adaptive time step stuff
theta = 1.1;
time_steps_used = zeros(num_tsteps, 1);
manu_soln_on = false;

[bigm, bigk] = getGlobalMassAndStiffness(c_prev_ts, numele, node, x);

time = 0;
mu_prev = NRmu(c_prev_ts, mu_prev, c_prev_ts, cdot_pre_ts, dt, alpha, D, gamma, bigm, bigk, numele, node, x);

if WRITE_VIDEO
    v = VideoWriter('ch-vid.avi');
    open(v);
end

% 2D heatmap of evolution
all_concs = zeros(num_tsteps, numnod);
times = zeros(num_tsteps, 1);

time_step = 0;
while time_step < num_tsteps

    [c, mu, cdot] = NRcombined(c_prev_ts, cdot_pre_ts, mu_prev, dt, alpha, ...
        D, gamma, bigm, bigk, numele, node, manu_soln_on, x, time + dt);
    
    compare_prev_step = norm(c - c_prev_ts);
    if compare_prev_step < 1e-2
        advance_step = true;
        dt = dt * theta;
    elseif compare_prev_step > 2e0
        % redo the previous time step while lowering dt
        advance_step = false;
        dt = dt / theta;
    else
        % keep everything the same
        advance_step = true;
    end
    if advance_step
        mu_prev = mu;
        c_prev_ts = c;
        cdot_pre_ts = cdot;
        time_step = time_step + 1;
        time = time + dt;
        time_steps_used(time_step) = dt;
        
        plot(x, c);
        titlestr = sprintf("1D CH: D = %f, time = %f, dt = %f, ts = %d", ...
                           D, time, dt, time_step);
        title(titlestr);
        axis([0 1 -1 1]);
        xlabel("position [m]");
        ylabel("relative concentration");
        drawnow;
        if WRITE_VIDEO
            frame = getframe(gcf);
            writeVideo(v, frame);
        end

        drawnow;

        all_concs(time_step, :) = c;
        times(time_step) = time;
    end
   
end

% plot all of the time steps
figure(2);
semilogy(1:num_tsteps, time_steps_used);
xlabel("time step");
ylabel("dt [s]");

if WRITE_VIDEO
    close(v);
end

figure(3);
% plot 2D heatmap of the results over time
h = imagesc([0, 1], [1, num_tsteps], all_concs);
xlabel("position [m]");
ylabel("time step");
colorbar
caxis([-1, 1])

% subplot of several time points
figure(4);
ts = [70, 530, 700, 1700];

subplot(2,2,1)
plot(x, all_concs(ts(1), :));
titlestr = sprintf("a) time = %e, ts=%d", times(ts(1)), ts(1));
title(titlestr);
axis([0 1 -1 1]);
xlabel("position [m]");
ylabel("relative concentration");

subplot(2,2,2)
plot(x, all_concs(ts(2), :));
titlestr = sprintf("b) time = %e, ts=%d", times(ts(2)), ts(2));
title(titlestr);
axis([0 1 -1 1]);
xlabel("position [m]");
ylabel("relative concentration");

subplot(2,2,3)
plot(x, all_concs(ts(3), :));
titlestr = sprintf("c) time = %e, ts=%d", times(ts(3)), ts(3));
title(titlestr);
axis([0 1 -1 1]);
xlabel("position [m]");
ylabel("relative concentration");

subplot(2,2,4)
plot(x, all_concs(ts(4), :));
titlestr = sprintf("d) time = %e, ts=%d", times(ts(4)), ts(4));
title(titlestr);
axis([0 1 -1 1]);
xlabel("position [m]");
ylabel("relative concentration");