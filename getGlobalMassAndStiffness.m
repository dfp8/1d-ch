function [bigm, bigk] = getGlobalMassAndStiffness(c, numele, node, x)

% initialize the mass matrix for reuse
% use 2 pt gaussian quadrature for the quadratic mass matrix
me = zeros(2, 2);
for eta = ((1 ./ sqrt(3)) .* [-1, 1])
    N = getN(eta);
    me = me + N' * N;
end
% pre-calculate ke
% ke = [1, -1; -1, 1];
B = [-0.5, 0.5];
ke = B' * B;

numnod = length(c);
bigk = zeros(numnod);
bigm = zeros(numnod);

%ELEMENT LOOP
for e=1:numele
    
    he = x(node(2,e)) - x(node(1,e));
    jacobian = 2 ./ he;  % jacobian
    ke_cur = jacobian .* ke;
    bigk(node(1,e),node(1,e)) = bigk(node(1,e),node(1,e)) + ke_cur(1,1);
    bigk(node(1,e),node(2,e)) = bigk(node(1,e),node(2,e)) + ke_cur(1,2);
    bigk(node(2,e),node(1,e)) = bigk(node(2,e),node(1,e)) + ke_cur(2,1);
    bigk(node(2,e),node(2,e)) = bigk(node(2,e),node(2,e)) + ke_cur(2,2);

    jacobian = he ./ 2;
    me_cur = me .* jacobian;
    bigm(node(1,e),node(1,e)) = bigm(node(1,e),node(1,e)) + me_cur(1,1);
    bigm(node(1,e),node(2,e)) = bigm(node(1,e),node(2,e)) + me_cur(1,2);
    bigm(node(2,e),node(1,e)) = bigm(node(2,e),node(1,e)) + me_cur(2,1);
    bigm(node(2,e),node(2,e)) = bigm(node(2,e),node(2,e)) + me_cur(2,2);
end
end