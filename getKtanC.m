function Ktan = getKtanC(me, numele, node, x, numnod)
bigm = zeros(numnod);

%ELEMENT LOOP
for e=1:numele
    he = x(node(2,e)) - x(node(1,e));
%     jacobian = 1 ./ he;  % jacobian
    jacobian = he ./ 2;

    me_cur = me .* jacobian;
    bigm(node(1,e),node(1,e)) = bigm(node(1,e),node(1,e)) + me_cur(1,1);
    bigm(node(1,e),node(2,e)) = bigm(node(1,e),node(2,e)) + me_cur(1,2);
    bigm(node(2,e),node(1,e)) = bigm(node(2,e),node(1,e)) + me_cur(2,1);
    bigm(node(2,e),node(2,e)) = bigm(node(2,e),node(2,e)) + me_cur(2,2);
end
Ktan = bigm;
end