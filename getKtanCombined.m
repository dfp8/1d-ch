function Ktan = getKtanCombined(c, bigm, bigk, numele, node, x, numnod, dt, D, gamma, alpha)
bigddf = zeros(numnod, numnod);

%ELEMENT LOOP
for e=1:numele
    he = x(node(2,e)) - x(node(1,e));
    jacobian = he ./ 2;

    % calculate the element force
    ce = [c(node(1,e)),  c(node(2,e))];
    fe = zeros(2, 2);
    locations = ((1 ./ sqrt(3)) .* [-1, 1]);
    weights = [1, 1];
    for eta_ind = 1:2
        eta = locations(eta_ind);
        N = getN(eta);
        ce_interp = ce * N';
        fe = fe + N' .* getDDPhi(ce_interp) * N .* weights(eta_ind) .* jacobian;
    end

    % assemble element force
    bigddf(node(1,e),node(1,e)) = bigddf(node(1,e),node(1,e)) + fe(1,1);
    bigddf(node(1,e),node(2,e)) = bigddf(node(1,e),node(2,e)) + fe(1,2);
    bigddf(node(2,e),node(1,e)) = bigddf(node(2,e),node(1,e)) + fe(2,1);
    bigddf(node(2,e),node(2,e)) = bigddf(node(2,e),node(2,e)) + fe(2,2);
end

Ktan = [(1 ./ (alpha .* dt)) .* bigm, D .* bigk; -bigddf - gamma .* bigk, bigm];

end

function ddphi = getDDPhi(c)
ddphi = 3 .* c .^ 2 - 1;
end