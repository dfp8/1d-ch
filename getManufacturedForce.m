function res = getManufacturedForce(x, alpha, t)
second = pi^2 .* alpha^2 .* (t + 1) .* sin(pi .* alpha .* x) .* (-pi^2 .* alpha^2 - ...
    3 .* (t + 1)^2 .* (sin(pi .* alpha .* x).^2 - 2 .* cos(pi .* alpha .* x).^2) + 1);
res = sin(pi .* alpha .* x) - second;
end