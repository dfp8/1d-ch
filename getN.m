function N = getN(eta)
N = 0.5 .* [(1 - eta), (1 + eta)];
end