clear;
clc;
close all

element_numbers = 2:10;
element_numbers = 2 .^ element_numbers;
interpolated_results = zeros(length(element_numbers), element_numbers(end)+1);

for size_ind = 1:length(element_numbers)
    numele = element_numbers(size_ind);
    numnod = numele+1;
    x = linspace(0,1,numnod);
    fprintf("Simulation with %d elements\n", numele);

    %nodal connectivity (LM) array
    node = [1:numele
       2:numele+1];

    % diffusion and gamma
    D = 1;
    gamma = 1;
    alpha = 1;

    c_prev_ts = zeros(numnod, 1);
    cdot_pre_ts = zeros(numnod, 1);
    mu_prev = zeros(numnod, 1);
    %initialize at first time step
    % for i=1:numnod
    %   c_prev_ts(i) = 0.01 .* sin(50 .* x(i));
    % end

    dt = 0.1;
    manu_soln_on = true;

    [bigm, bigk] = getGlobalMassAndStiffness(c_prev_ts, numele, node, x);

    time = 0;

    % run manufactured soln
    [c, mu, cdot] = NRcombined(c_prev_ts, cdot_pre_ts, mu_prev, dt, alpha, ...
        D, gamma, bigm, bigk, numele, node, manu_soln_on, x, dt);
    
    % store interpolated results
    interpolated_results(size_ind, :) = interp1(x, c, ...
        linspace(0,1,element_numbers(end)+1), 'linear');
   fprintf("\n");
end

figure(1);
plot(x, c);
% titlestr = sprintf("Most elements and manufactured soln\n");
% title(titlestr);
xlabel("position [m]");
ylabel("relative concentration");
c_manufactured = (dt + 1) .* sin(1 .* pi .* x);
hold on
plot(x, c_manufactured);
legend("FEM", "manufactured");

% calculate L2 norms of the error
errors = zeros(length(element_numbers), 1);
for size_ind = 1:length(element_numbers)
    errors(size_ind) = norm(interpolated_results(size_ind, :) - c_manufactured, 2);
end
figure(2);
subtracted_errors = errors - errors(end);
loglog(element_numbers, subtracted_errors, "ko-");
xlabel("$n_{elements}$", "Interpreter", "latex");
ylabel("$|L_2$error - $L_2$finest$|$", "Interpreter", "latex");
hold on
% add order 2 line
x_trendline = linspace(log10(element_numbers(1))-0.5, log10(element_numbers(end))+0.5);
b = log10(subtracted_errors(1)) + 2 .* (log10(element_numbers(1)));
polynomial = [-2, b];
y_vals = polyval(polynomial, x_trendline);
loglog(10 .^ x_trendline, 10 .^ y_vals, "k--");
legend("simulation", "order 2");